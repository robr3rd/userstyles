# UserStyles
This is just a place to store helpful UserStyles.  Includes UserStyle files that are from-scratch, customized, or just generally from the wonderful community.

As should be expected from an informal "personal brain dump storage" repo such as this one, no guarantees are made that any of these will work for you or will be kept up-to-date.


## How are things organized?
Files are arranged using the following logic:

- Filenames:
	- Simple: `[service_name]-[what_it_does].css`
	- Specific: `[service_name]-[affected_components]-[what_it_does].css`
- Service has multiple files: `[service_name]/[filename]`
- Service's _component_ has multiple files: `[service_name]/[component_name]/[filename]`


## How do I use these?
1. Open the one you want.
1. Copy source code to clipboard.
1. "Import" into a tool like Stylus that supports the UserStyles syntax/format.

That's it.  You're done.  Maybe you have to hit an "enable" button in your UserStyles manager, but that's all "tool stuff" at that point.

Enjoy!



## Contributing
Nothing is expected but feel free to open Pull Requests, etc.  :)


## Licensing
For this project, see [LICENSE](LICENSE) ("BSD-2-Clause").  Attribution is appreciated but absolutely not required.

Less clear-cut are any UserStyles that are copied wholesale or were extended.  When present, I've made every effort to retain original in-file comments, etc., but unfortunately people don't seem to consider the inclusion of author details much less providing licenses for these things.  As such, the assumed license for anything without explicit license/ownership must be assumed to be the terribly unclear, implied license(s) that apply to all random code on the Internet.

